/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controller;

import java.io.IOException;

/**
 *
 * @author lenindfabricio
 */
public class ComandExecute {
     public Process executeComand(String comandShell) {
        Process process = null;
        System.out.println("Ejecucion en proceso \n Espere....");
        try {
            String[] arrayComands = {"bash", "-c", comandShell};
            process = Runtime.getRuntime().exec(arrayComands);
        } catch (IOException e) {
            System.err.println("Excepcion presentada "+e.getLocalizedMessage());
        }

        return process;
    }
}
