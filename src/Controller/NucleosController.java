/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import javax.swing.JLabel;

/**
 *
 * @author lenindfabricio
 */
public class NucleosController {

     public void getNucleos(JLabel labelNucleos) {
        ReadProcess p = new ReadProcess();
        ComandExecute comand = new ComandExecute();
        Process po = comand.executeComand("lscpu -p | egrep -v '^#' | sort -u -t, -k 2,4 | wc -l");
        String line = "";
        Integer cantNucleos = Integer.parseInt(p.readProcess(po).trim());
        for(int i = 0; i < cantNucleos; i++) {
            line=String.valueOf(cantNucleos);
        }
        
        labelNucleos.setText(line);
    }
    
     
}
