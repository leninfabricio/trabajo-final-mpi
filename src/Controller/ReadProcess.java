/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author lenindfabricio
 */
public class ReadProcess {
    public String readProcess(Process proceso) {
        String nameProcess ="";
        BufferedReader entryProcess = null;
        try {
            entryProcess = new BufferedReader(new InputStreamReader(proceso.getInputStream()));
            String line = entryProcess.readLine();
            while (line != null) {
                nameProcess += line + "\n";
                line = entryProcess.readLine();
            }
        } catch (IOException e) {
            System.err.println("Excepcion presentada "+e.getLocalizedMessage());
        }
        return nameProcess;
    }
}
