/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package java_mpi;

import Views.Ventana;

/**
 *
 * @author lenindfabricio
 */
public class JAVA_MPI {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Ventana v1 = new Ventana();
        v1.setVisible(true);
        v1.setLocationRelativeTo(null);
        v1.setResizable(false);
    }
    
}
